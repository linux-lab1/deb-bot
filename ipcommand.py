#!/usr/bin/env python3
import socket

import requests,ipaddress
#inspired by BlackIp eggdrop tcl script


def check_ip_command(arg):
    result=[]
    try:
        ip = ipaddress.ip_address(arg)
        result=ip_info(arg)
    except ValueError:
        result=hostname(arg)
    return result

def ip_info(ip):
    try:
        r=requests.get(f'http://ip-api.com/json/{ip}?fields=country,regionName,city,lat,lon,timezone,mobile,proxy,query,reverse,status,message,isp')
    except:
        r = requests.get(f'https://ip-api.io/api/json/{ip}')
    return output(r.json())


def hostname(url):
    try:
      ip=socket.gethostbyname(url)
      result=ip_info(ip)
    except:
        try:
            ip=socket.getaddrinfo((url), 80, family=socket.AF_INET6, proto=socket.IPPROTO_TCP)[0][4][0]
            result = ip_info(ip)
        except:
            result=[]
    return result
    #return output(r.json())
    # return(str(r.json()))


def output(d):
    try:
        if 'mobile' in d:
            # result=str(d)
            result = f"\002{d['query']}\002 is located in \002{d['city']}\002, \002{d['regionName']}\002, \002{d['country']}\002 (\002{d['lat']}\002, \002{d['lon']}\002) ; "
            result += f"TimeZone: \002{d['timezone']}\002 ; ISP: \002{d['isp']}\002 ; Mobile: \002{d['mobile']}\002 ; Proxy: \002{d['proxy']}\002"
        else:
             result=f"\002{d['ip']}\002 is located in \002{d['city']}\002, \002{d['region_name']}\002, \002{d['country_name']}\002 (\002{d['latitude']}\002, \002{d['longitude']}\002) ; "
             result+=f"TimeZone: \002{d['time_zone']}\002 ; ISP: \002{d['organisation']}\002"
        result_list=[result]
    except:
        result_list=[]
    return result_list

if __name__ == "__main__":
    print(check_ip_command('debian.theserver.tk'))
