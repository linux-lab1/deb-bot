from lxml import html
import requests

# Define the search function of the xpath
def get_info_package(namepkg):
    result=[]
    try:
        page = requests.get(f'https://packages.debian.org/{namepkg}')
        tree = html.fromstring(page.content)
        bullseye = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"bullseye")]')
        bookworm = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"bookworm")]')
        sid = tree.xpath('//*[@id="psearchres"]/ul[1]/li[contains(@class,"sid")]')
        x=bullseye[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(f"{namepkg}: {y[1]}")
        result.append(y[0]+ " " +y[2])
        x=bookworm[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(y[0]+ " " +y[2])
        x=sid[0].text_content().split('\n')
        y=[a.strip() for a in x if a.strip()]
        result.append(y[0]+ " " + y[2])
    except IndexError:
        result=["Unknown Packet"]
        return result

    return result


if __name__ == "__main__":
    print(get_info_package('mc'))
