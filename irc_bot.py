#!/usr/bin/env python3

# This bot reads news from a couple of sources
# I found here the basic bot code: https://www.techbeamers.com/create-python-irc-bot/
# on this I have built my news reader

from irc_class import *
import os,time
import datetime
import getfeeds ,irccommands,ipcommand, pckg
import configparser
import argparse
import logging, logging.handlers
from msg_parser import MsgParser

def starts_with(text,start):
    l_start=len(start)
    start_text=text.strip()[:l_start]
    result=start_text==start
    return result


def ends_with(text,end):
    l_end=len(end)
    end_text=text.strip()[-l_end:]
    result=end_text==end
    return result


def print_feeds(channel,feeds):
    logger.info("print_feeds")
    for feed in reversed(feeds):
        irc.send(channel, feed)
        time.sleep(1)

def check_news():
    logger.info("check_news")
    s=dsa.get_new_feeds()
    print_feeds(channel, s)
    s=planet.get_new_feeds()
    print_feeds(channel, s)

def get_logger(fn):
    logger = logging.getLogger('debbot')
    logger.setLevel(logging.DEBUG)
    # handler = logging.handlers.SysLogHandler(address='/dev/log')
    # handler = logging.FileHandler(fn)
    # formatter = logging.Formatter('%(asctime)s - %(name)s: %(levelname)s - %(message)s')
    # handler.setFormatter(formatter)
    # logger.addHandler(handler)
    # handler = logging.handlers.TimedRotatingFileHandler(fn, 'D', 1, 7)
    handler = logging.handlers.TimedRotatingFileHandler(fn, when='d', interval=1, backupCount=7)
    formatter = logging.Formatter('%(asctime)s - %(name)s: %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger

def ip_command(arg):
    if arg==botnick:
        #hide bot info
        return
    result=ipcommand.check_ip_command(arg)
    if not result:
        arg1=irc.info_nick(arg)
        if arg1:
            result = ipcommand.check_ip_command(arg1)
            if result:
                result.insert(0,f'Nick: \002{arg}\002 ; Host: \002{arg1}\002')
    if result:
        for x in result:  
            irc.send(channel, x) 
    else:
       irc.send(channel, "unknown host/nick")

def get_bot_uptime():
    now = datetime.datetime.utcnow()
    delta = now - bot_connection_time
    hours, remainder = divmod(int(delta.total_seconds()), 3600)
    minutes, seconds = divmod(remainder, 60)
    days, hours = divmod(hours, 24)


    if days:
        fmt = '{d} days, {h} hours, {m} minutes, and {s} seconds'
    else:
        fmt = '{h} hours, {m} minutes, and {s} seconds'

    return fmt.format(d=days, h=hours, m=minutes, s=seconds)


def dpkg_command(arg):
    result=pckg.get_info_package(arg)
    for x in result:
        irc.send(channel, x.replace("\n", " "))


if __name__ == "__main__":

    BOT_VERSION="1.4.0"
    RELEASE_DATE="2022-05-21"
    version_str=f"Deb-Bot {BOT_VERSION} ({RELEASE_DATE})"
    cf=os.path.join(os.path.dirname(sys.argv[0]),'debbot.conf')
    lf = os.path.join(os.path.dirname(sys.argv[0]), 'debbot.log')
    cmdfile = os.path.join(os.path.dirname(sys.argv[0]), 'irc_commands.json')

    parser = argparse.ArgumentParser(description='Debian News Irc Bot')
    parser.add_argument('-c','--config', type=str,default=cf)
    parser.add_argument('-l', '--logfile', type=str, default=lf)
    parser.add_argument( '--commandfile', type=str, default=cmdfile)
    parser.add_argument('--version', action='version', version=version_str)
    args = parser.parse_args()

    conf=args.config
    logger=get_logger(args.logfile)

    cp=configparser.ConfigParser()
    cp.read(conf)



    ## IRC Config
    # server = "irc.libera.chat"  # Provide a valid server IP/Hostname
    server=cp['DEFAULT']['server']
    port = int(cp['DEFAULT']['port'])  #6667
    channel = cp['DEFAULT']['channel'] ##bot-testing"

    botnick=cp['DEFAULT']['botnick']
    botnickpass=cp['DEFAULT']['botnickpass']
    botpass=cp['DEFAULT']['botpass']
    ipv6=cp['DEFAULT']['ipv6']=='Y'

    if ipv6:
        family = socket.AF_INET6
    else:
        family = socket.AF_INET

    check_every_secs=int(cp['DEFAULT']['check_every_secs'])#300
    t=time.time()-10000

    irc = IRC(family,logger)
    #commands
    command_file=args.commandfile

    try:
        commands=irccommands.IRC_commands(command_file,logger)
    except Exception as e:
        logger.error(f"Problem loading commmand file {command_file}: {e}")
        commands = None

    #connection
    irc.connect(server, port, channel, botnick, botnickpass, botpass)
    bot_connection_time=datetime.datetime.utcnow()


    #feed sources
    dsa_url = "https://www.debian.org/security/dsa-long"
    planet_url = "https://planet.debian.org/rss20.xml"
    dsa=getfeeds.GetNewFeeds(dsa_url,"dsa")
    planet=getfeeds.GetNewFeeds(planet_url,"planet")


    loopCondition=True

    #main loop
    while loopCondition:
        irc.settimeout(check_every_secs-(time.time()-t)+1)
        parsed_msg= irc.get_response()
        # parsed_msg=MsgParser(text)
        #print(text)
        # logger.info(text)
        if parsed_msg.command=="PRIVMSG" and channel==parsed_msg.args[0]:
            if  len(parsed_msg.args)==2:
                arg2 = parsed_msg.args[1]
                if arg2=="hello":
                    irc.send(channel, f"Hello {parsed_msg.get_nick()}!")
                elif arg2=="!dsa":
                    print_feeds(channel, getfeeds.get_feeds(dsa_url))
                elif starts_with(arg2,"!dpkg"):
                    arg2n=arg2.split(' ')[1:]
                    if len(arg2n)==1:
                        dpkg_command(arg2n[0])
                elif starts_with(arg2,"!ip"):
                    arg2n=arg2.split(' ')[1:]
                    if len(arg2n)==1:
                        ip_command(arg2n[0])
                elif arg2=="!planet":
                    print_feeds(channel, getfeeds.get_feeds(planet_url))
                elif arg2=="!botversion":
                    irc.send(channel, version_str)
                elif arg2 == "!botuptime":
                    irc.send(channel, get_bot_uptime())
                # commands
                elif commands:
                    cmd=commands.check_and_execute(parsed_msg,botnick)
                    if cmd:
                         irc.send(channel, cmd)

            # elif ("!exit" in text):
            #     loopCondition=False
        if time.time()-t > check_every_secs:
            check_news()
            t = time.time()

